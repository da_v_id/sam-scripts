#! /usr/bin/env node

const { writeFileSync, unlinkSync, readFileSync } = require("fs");
const jsYaml = require("js-yaml");
const { schema } = require("yaml-cfn");
const { execSync } = require("child_process");
const AWS = require("aws-sdk");

const createTable = (params) => {
  writeFileSync("db.json", JSON.stringify(params));
  let command = `aws dynamodb delete-table --table-name ${params.TableName}  --endpoint-url http://localhost:8000`;

  try {
    execSync(command);
  } catch (error) {
    console.log("No table exists");
  }

  command = `aws dynamodb create-table --endpoint-url http://localhost:8000 --cli-input-json file://db.json`;
  execSync(command, { stdio: [0, 1, 2] });

  unlinkSync("db.json");

  loadData(params.TableName);
};

const loadData = async (tablename) => {
  try {
    const dataFile = `sample-data/${tablename}.json`;
    const items = JSON.parse(readFileSync(dataFile, "utf8"));
    await Promise.all(
      items.map(async (item) => {
        insertIntoDB(item, tablename);
      })
    );
  } catch (error) {
    console.log(error);
  }
};

const insertIntoDB = async (item, name) => {
  let docClient = new AWS.DynamoDB.DocumentClient();

  const awsRegion = "eu-west-1";
  const localEndpoint = "http://localhost:8000";
  const options = {
    apiVersion: "2012-08-10",
    endpoint: localEndpoint,
    region: awsRegion,
  };
  const documentClientOptions = {
    service: new AWS.DynamoDB(options),
  };
  docClient = new AWS.DynamoDB.DocumentClient(documentClientOptions);

  let params = {
    Item: item,
    TableName: name,
  };

  await docClient
    .put(params)
    .promise()
    .then(() => {
      return true;
    })
    .catch((err) => {
      console.log(err);
    });
};

const templateYaml = jsYaml.load(readFileSync("template.yaml", "utf8"), {
  schema: schema,
});

const dynamoDatabases = [];

for (let resource in templateYaml.Resources) {
  if (templateYaml.Resources[resource].Type === "AWS::DynamoDB::Table") {
    dynamoDatabases.push(templateYaml.Resources[resource]);
  }
}

dynamoDatabases.forEach((db) => {
  db.Properties.ProvisionedThroughput = {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  };

  db.Properties.SSESpecification.Enabled =
    db.Properties.SSESpecification.SSEEnabled;
  delete db.Properties.SSESpecification.SSEEnabled;

  delete db.Properties.PointInTimeRecoverySpecification;
  delete db.Properties.Tags;
  delete db.Properties.BillingMode;

  createTable(db.Properties);
});
